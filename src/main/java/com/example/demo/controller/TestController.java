package com.example.demo.controller;

import com.example.demo.model.ModelDTO;
import com.example.demo.repo.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class TestController {

    private ModelRepository modelRepository;

    @Autowired
    public TestController(ModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    @PostMapping(value = "/{modelCategory}/invoke")
    public ModelDTO callModel(@RequestBody Map modelInput, @PathVariable("modelCategory") String modelCategory,
                              HttpServletRequest request) {
        List<ModelDTO> models = modelRepository.findModelDTOByModelCategory(modelCategory).orElse(new ArrayList<>());

        ModelDTO model = models.stream().filter(m -> new HashSet<>(m.getParams()).equals(request.getParameterMap().keySet()))
                .findFirst().orElse(null);

        String params = request.getParameterMap().entrySet().stream()
                .map(e -> String.format("%s=%s", e.getKey(), Stream.of(e.getValue())
                        .map(String::valueOf).collect(Collectors.joining(","))))
                .collect(Collectors.joining("&"));
        String uri = String.format("%s?%s", model.getEndpoint(), params);
        model.setEndpoint(uri);
        return model;
    }
}

package com.example.demo.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ModelDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String modelName;
    private String endpoint;
    @ElementCollection
    private List<String> params = new ArrayList<>();
    private String modelCategory;

    public ModelDTO() {
    }

    public ModelDTO(String modelName, String modelCategory, String endpoint, List<String> params) {
        this.modelName = modelName;
        this.modelCategory = modelCategory;
        this.endpoint = endpoint;
        this.params = params;
    }

    public String getModelCategory() {
        return modelCategory;
    }

    public void setModelCategory(String modelCategory) {
        this.modelCategory = modelCategory;
    }

    public List<String> getParams() {
        return params;
    }

    public void setParams(List<String> params) {
        this.params = params;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }
}

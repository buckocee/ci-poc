package com.example.demo.model;

public class TestDTO {

    private Type status;

    public Type getStatus() {
        return status;
    }

    public void setStatus(Type status) {
        this.status = status;
    }

    public enum Type {
        PENDING, COMPLETE, FAILED
    }
}

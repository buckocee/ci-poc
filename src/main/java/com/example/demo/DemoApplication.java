package com.example.demo;

import com.example.demo.model.ModelDTO;
import com.example.demo.repo.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class DemoApplication implements ApplicationRunner {

    @Autowired
    private ModelRepository modelRepository;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        ModelDTO nn = new ModelDTO("Negative News", "NegativeNews", "nn/endpoint/v1",
                Arrays.asList("firstName", "lastName"));
        ModelDTO nn_bulk = new ModelDTO("Negative News", "NegativeNews", "nn/bulk/endpoint/v1",
                Arrays.asList("startDate", "endDate"));
        ModelDTO ars = new ModelDTO("Alert Risk Scoring", "AlertRiskScoring", "ars/endpoint/v1",
                null);
        ModelDTO ng = new ModelDTO("Narrative Generation", "NarrativeGeneration", "ng/endpoint/v1",
                Arrays.asList("startDate", "endDate"));

        modelRepository.saveAll(Arrays.asList(nn, nn_bulk, ars, ng));

    }
}

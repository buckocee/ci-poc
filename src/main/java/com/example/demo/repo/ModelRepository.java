package com.example.demo.repo;

import com.example.demo.model.ModelDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ModelRepository extends JpaRepository<ModelDTO, Integer> {

    Optional<List<ModelDTO>> findModelDTOByModelCategory(@Param("modelCategory") String modelCategory);
}
